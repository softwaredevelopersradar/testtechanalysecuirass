﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TopPanel
{
    /// <summary>
    /// Логика взаимодействия для TopPanelControl.xaml
    /// </summary>
    public partial class TopPanelControl : UserControl
    {
        public event EventHandler OnCheckLocalProp;
        public event EventHandler OnUnCheckLocalProp;        
        public event EventHandler OnStart;

        public TopPanelControl()
        {
            InitializeComponent();
            
        }

        private void bStart_Click(object sender, RoutedEventArgs e)
        {
            OnStart?.Invoke(this, null);
        }

        private void bLocalProp_Checked(object sender, RoutedEventArgs e)
        {
            OnCheckLocalProp?.Invoke(this, null);
        }

        private void bLocalProp_Unchecked(object sender, RoutedEventArgs e)
        {
            OnUnCheckLocalProp?.Invoke(this, null);
        }

        private void bLocalProp_Click(object sender, RoutedEventArgs e)
        {
            OnCheckLocalProp?.Invoke(this, null);
        }
    }
}
