﻿using Arction.Wpf.SemibindableCharting;
using System;
using System.Windows.Controls;
using System.Numerics;
using System.Linq;
using System.Collections.Generic;

namespace DrawIQCounts
{
    /// <summary>
    /// Логика взаимодействия для DrawIQControl.xaml
    /// </summary>
    public partial class DrawIQControl : UserControl
    {
        double[] PhaseArray = new double[2048];
        double[] AmplitudeArray = new double[2048];
        double max;
        double min;
        double maxAmplitude;

        string nameAxP = "";
        public string NameAxP
        {
            get => nameAxP;
            set
            {
                nameAxP = value;
                yPTitle.Text = nameAxP;
            }
        }

        int lengthArray = 2048;
        int LengthArray
        {
            get => lengthArray;
            set
            {
                lengthArray = value;
            }
        }
        double[] array;
        double[] arrayFrq;
        double[] arrayAmp;

        Int16 RowCount = 1;
        int length = 16384;
        public int LengthSG
        {
            get => length;
            set
            {
                length = value;
                data = new double[length][];
                for (int col = 0; col < length; col++)
                {
                    data[col] = new double[RowCount];
                }
                spectrogram.Clear();
                spectrogram.SetValuesData(data, IntensityGridValuesDataOrder.ColumnsRows);
            }
        }
        double[][] data;
        
        public DrawIQControl()
        {
            InitializeComponent();
            string deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);

            tbPName.Text = "\u03C6," + "\u00B0" + " :";            
        }

        private void InitSpectrogram()
        {
            graphFreqSpec.BeginUpdate(); 

            var view = graphFreqSpec.ViewXY;
            data = new double[length][];
            for (int col = 0; col < 16384; col++)
            {
                data[col] = new double[RowCount];
            }

            spectrogram.SetValuesData(data, IntensityGridValuesDataOrder.ColumnsRows);
            spectrogram.PixelRendering = true;
            spectrogram.ContourLineStyle.AntiAliasing = LineAntialias.None;
            //spectrogram.ContourLineType = ContourLineTypeXY.None;
            spectrogram.Title.Text = "";
            spectrogram.SetRangesXY(view.XAxes[0].Minimum, view.XAxes[0].Maximum,
                view.YAxes[0].Minimum, view.YAxes[0].Maximum);

            graphFreqSpec.EndUpdate();
        }

        public void DrawAmp(double[] amplitudeArray)
        {
            DispatchIfNecessary(() => 
            {
                var data = new SeriesPoint[amplitudeArray.Length];
                arrayAmp = new double[amplitudeArray.Length];
                var _view = graphModule.ViewXY;
                double delay = 0.0145416259765625; //0.0146484375
                FindMax(amplitudeArray);
                maxAmplitude = max;

                for (int i = 0; i < amplitudeArray.Length; i++)
                {
                    data[i].X = delay * i;
                    data[i].Y = amplitudeArray[i];
                    arrayAmp[i] = data[i].Y;
                }
                
                graphModule.BeginUpdate();

                XLengthMin = 0;
                XLengthMax = delay * data.Length;
                YLengthMin = 0;
                YLengthMax = max + 250;

                XAxAmp.Minimum = 0;
                XAxAmp.Maximum = delay * data.Length;
                YAxAmp.Minimum = 0;
                YAxAmp.Maximum = max + 250;
                _view.PointLineSeries[0].Points = data;

                CursorThd.Value = Threshold * max;

                graphModule.EndUpdate();
            });            
        }

        public void DrawGraphPhase(double[] phaseArray)
        {
            DispatchIfNecessary(() => 
            {
                var data = new SeriesPoint[phaseArray.Length];
                LengthArray = phaseArray.Length;
                array = new double[lengthArray];
                var _view = graphPhase.ViewXY;
                double delay = 0.0145416259765625;
                FindMax(phaseArray);
                FindMin(phaseArray);

                for (int i = 0; i < phaseArray.Length; i++)
                {
                    data[i].X = delay * i;
                    data[i].Y = phaseArray[i];
                    array[i] = data[i].Y;
                }

                graphPhase.BeginUpdate();

                XPhaseLengthMin = 0;
                XPhaseLengthMax = delay * data.Length;
                YPhaseLengthMin = min - 20;
                YPhaseLengthMax = max + 20;

                XAxPhase.Minimum = 0;
                XAxPhase.Maximum = delay * data.Length;
                YAxPhase.Minimum = min - 20;
                YAxPhase.Maximum = max + 20;
                _view.PointLineSeries[0].Points = data;

                graphPhase.EndUpdate();
            });            
        }

        public void DrawGraphFreq(double[] freqArray)
        {
            DispatchIfNecessary(() => 
            {
                var data = new SeriesPoint[freqArray.Length];
                var _view = graphFreq.ViewXY;
                arrayFrq = new double[freqArray.Length];
                double delay = 0.0145416259765625;
                FindMin(freqArray);
                FindMax(freqArray);
                for (int i = 0; i < freqArray.Length; i++)
                {
                    data[i].X = delay * i;
                    data[i].Y = freqArray[i];
                    arrayFrq[i] = data[i].Y;
                }

                graphFreq.BeginUpdate();

                XFreqLengthMin = 0;
                XFreqLengthMax = delay * data.Length;

                YAxFreq.Minimum = YFreqLengthMin;
                YAxFreq.Maximum = YFreqLengthMax;
                XAxFreq.Minimum = 0;
                XAxFreq.Maximum = delay * data.Length;
                _view.PointLineSeries[0].Points = data;

                graphFreq.EndUpdate();

            });
        }

        public void DrawGrapfSpectrogram(double[] freqArray)
        {
            DispatchIfNecessary(() => 
            {
                double delay = 0.0145416259765625;

                graphFreqSpec.BeginUpdate();

                var view = graphFreqSpec.ViewXY;

                XSpLengthMin = 0;
                XSpLengthMax = delay * freqArray.Length;
                YSpLengthMin = 0;
                YSpLengthMax = RowCount;

                YAxSp.Minimum = 0;
                YAxSp.Maximum = RowCount;
                XAxSp.Minimum = 0;
                XAxSp.Maximum = delay * freqArray.Length;
                
                for (int i = 0; i < freqArray.Length-1; i++)
                {
                    for (int j =0; j < RowCount; j++)
                    {
                        data[i][j] = freqArray[i];
                    }
                }

                spectrogram.SetRangesXY(view.XAxes[0].Minimum, view.XAxes[0].Maximum,
                    view.YAxes[0].Minimum, view.YAxes[0].Maximum);
                
                spectrogram.InvalidateValuesDataOnly();
                graphFreqSpec.EndUpdate();
            });
        }

        private void FindMax(double[] array)
        {
            max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (max < array[i])
                {
                    max = array[i];
                }
            }
        }

        private void FindMin(double[] array)
        {
            min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                }
            }
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

    }
}
