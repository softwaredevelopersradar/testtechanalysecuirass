﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DrawIQCounts
{
    public partial class DrawIQControl
    {
        string text;

        public delegate void OnSendFimpValue(string text);
        public event OnSendFimpValue OnSubmittedFimpValue = (text) => { };
        private void tbFimp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int pos = tbFimp.SelectionStart;
            //OnSubmittedFimpValue?.Invoke(tbFimp.Text);

            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
                    && (!tbFimp.Text.Contains(",")
                    && tbFimp.Text.Length != 0)))
            {
                e.Handled = true;
                return;
            }
            string text = tbFimp.Text.Insert(pos, e.Text);  //e.Text
            OnSubmittedFimpValue?.Invoke(text);
        }

        private void tbFimp_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (e.Key == Key.Enter)  // || e.Key == Key.OemMinus
            {
                //OnSubmittedFimpValue?.Invoke(tbFimp.Text);

                //double.TryParse(tbFimp.Text, out Freq);
                Keyboard.ClearFocus();
            }
            if (e.Key == Key.OemMinus)
            {
                if(tbFimp.SelectionStart == 0)
                e.Handled = false;
            }
        }

        private void tbFimp_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }
    }
}
