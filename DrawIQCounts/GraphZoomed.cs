﻿using Arction.Wpf.SemibindableCharting.SeriesXY;
using Arction.Wpf.SemibindableCharting.Views.ViewXY;
using System;
using System.Windows.Controls;

namespace DrawIQCounts
{
    public partial class DrawIQControl : UserControl
    {
        public delegate void BandDoubleEventHandler(double bLeft, double bWidth, double bRight);
        public event BandDoubleEventHandler OnBandData = (left, width, max) => { };
        public event BandDoubleEventHandler OnBandPhase = (left, width, max) => { };
        public event BandDoubleEventHandler OnBandFreq = (left, width, max) => { };

        double YLengthMin = 0;
        double YLengthMax = 5000;
        double XLengthMin = 0;
        double XLengthMax = 30;

        double YPhaseLengthMin = -3;
        double YPhaseLengthMax = 3;
        double XPhaseLengthMin = 0;
        double XPhaseLengthMax = 30;

        double YFreqLengthMin = -50;
        double YFreqLengthMax = 50;
        double XFreqLengthMin = 0;
        double XFreqLengthMax = 30;

        double YSpLengthMin = 0;
        double YSpLengthMax = 20;
        double XSpLengthMin = 0;
        double XSpLengthMax = 30;

        bool xAmpChanged = false;
        bool xPhChanged = false;
        bool xFreqChanged = false;

        bool bandAmp = false;
        bool bandPhase = false;
        bool bandFreq = false;

        #region AxysLengthChange
        private void YAxAmp_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = YAxAmp;
                if (e.NewMax - e.NewMin > YLengthMax - YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                    return;
                }
                if (e.NewMax > YLengthMax)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
            }
            catch { }
        }

        private void XAxAmp_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = XAxAmp;

                if (e.NewMax - e.NewMin > XLengthMax - XLengthMin)
                {
                    aXysX.SetRange(XLengthMin, XLengthMax);
                    return;
                }
                if (e.NewMin < XLengthMin)
                {
                    double shift = XLengthMin - e.NewMin;
                    aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
                }
                if (e.NewMax > XLengthMax)
                {
                    double shift = e.NewMax - XLengthMax;
                    aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
                }

                xAmpChanged = true;

                if (xPhChanged != true && xFreqChanged != true)
                {
                    XAxPhase.Minimum = aXysX.Minimum;
                    XAxPhase.Maximum = aXysX.Maximum;
                    XAxFreq.Minimum = aXysX.Minimum;
                    XAxFreq.Maximum = aXysX.Maximum;
                }

                xAmpChanged = false;
            }
            catch { }
        }

        private void YAxPhase_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = YAxPhase;
                if (e.NewMax - e.NewMin > YPhaseLengthMax - YPhaseLengthMin)
                {
                    aXysY.SetRange(YPhaseLengthMin, YPhaseLengthMax);
                    return;
                }
                if (e.NewMax > YPhaseLengthMax)
                {
                    aXysY.SetRange(YPhaseLengthMin, YPhaseLengthMax);
                }
                if (e.NewMin < YPhaseLengthMin)
                {
                    aXysY.SetRange(YPhaseLengthMin, YPhaseLengthMax);
                }
            }
            catch { }
        }

        private void XAxPhase_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = XAxPhase;
                if (e.NewMax - e.NewMin > XPhaseLengthMax - XPhaseLengthMin)
                {
                    aXysX.SetRange(XPhaseLengthMin, XPhaseLengthMax);
                    return;
                }
                if (e.NewMin < XPhaseLengthMin)
                {
                    double shift = XPhaseLengthMin - e.NewMin;

                    aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
                }
                if (e.NewMax > XPhaseLengthMax)
                {
                    double shift = e.NewMax - XPhaseLengthMax;
                    aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
                }

                xPhChanged = true;

                if (xAmpChanged != true && xFreqChanged != true) 
                {
                    XAxAmp.Minimum = aXysX.Minimum;
                    XAxAmp.Maximum = aXysX.Maximum;
                    XAxFreq.Minimum = aXysX.Minimum;
                    XAxFreq.Maximum = aXysX.Maximum;
                }

                xPhChanged = false;
            }
            catch { }
        }

        private void YAxFreq_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = YAxFreq;
                if (e.NewMax - e.NewMin > YFreqLengthMax - YFreqLengthMin)
                {
                    aXysY.SetRange(YFreqLengthMin, YFreqLengthMax);
                    return;
                }
                if (e.NewMax > YFreqLengthMax)
                {
                    aXysY.SetRange(YFreqLengthMin, YFreqLengthMax);
                }
                if (e.NewMin < YFreqLengthMin)
                {
                    aXysY.SetRange(YFreqLengthMin, YFreqLengthMax);
                }
            }
            catch { }
        }

        private void XAxFreq_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = XAxFreq;
                if (e.NewMax - e.NewMin > XFreqLengthMax - XFreqLengthMin)
                {
                    aXysX.SetRange(XFreqLengthMin, XFreqLengthMax);
                    return;
                }
                if (e.NewMin < XFreqLengthMin)
                {
                    double shift = XFreqLengthMin - e.NewMin;

                    aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
                }
                if (e.NewMax > XFreqLengthMax)
                {
                    double shift = e.NewMax - XFreqLengthMax;
                    aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
                }

                xFreqChanged = true;

                if (xAmpChanged != true && xPhChanged != true) 
                {
                    XAxAmp.Minimum = aXysX.Minimum;
                    XAxAmp.Maximum = aXysX.Maximum;
                    XAxPhase.Minimum = aXysX.Minimum;
                    XAxPhase.Maximum = aXysX.Maximum;
                }

                xFreqChanged = false;
            }
            catch { }
        }

        private void YAxSp_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = YAxSp;
                if (e.NewMax - e.NewMin > YSpLengthMax - YSpLengthMin)
                {
                    aXysY.SetRange(YSpLengthMin, YSpLengthMax);
                    return;
                }
                if (e.NewMax > YSpLengthMax)
                {
                    aXysY.SetRange(YSpLengthMin, YSpLengthMax);
                }
                if (e.NewMin < YSpLengthMin)
                {
                    aXysY.SetRange(YSpLengthMin, YSpLengthMax);
                }
            }
            catch { }
        }

        private void XAxSp_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            var aXysX = XAxSp;
            if (e.NewMax - e.NewMin > XSpLengthMax - XSpLengthMin)
            {
                aXysX.SetRange(XSpLengthMin, XSpLengthMax);
                return;
            }
            if (e.NewMin < XSpLengthMin)
            {
                double shift = XSpLengthMin - e.NewMin;

                aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
            }
            if (e.NewMax > XSpLengthMax)
            {
                double shift = e.NewMax - XSpLengthMax;
                aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
            }
        }
        #endregion

        #region Bands
        private void BandAmp_ValuesChanged(object sender, ValuesChangedEventArgs e)
        {
            double begin = e.NewBeginValue;
            double width = e.NewEndValue - e.NewBeginValue;
            double end = e.NewEndValue;
            bool isLimit = false;

            if (width < 0)
            {
                width *= (-1);
                begin = e.NewEndValue;
                end = e.NewBeginValue;
            }

            if (e.NewEndValue > XLengthMax) 
            {
                end = XLengthMax;
                width = Math.Abs(end - e.NewBeginValue);
            }
            if (e.NewBeginValue < XLengthMin) 
            {
                begin = XLengthMin;
                width = Math.Abs(e.NewEndValue - begin);
            }
            OnBandData?.Invoke(begin, width, end);

            tbLeft.Text = string.Format("{0:0.0}", begin);
            tbWidth.Text = string.Format("{0:0.0}", width);
            tbRight.Text = string.Format("{0:0.0}", end);

            bandAmp = true;
            if (bandPhase != true && bandFreq != true) 
            {
                if (BandPhase != null)
                {
                    BandPhase.ValueBegin = begin;
                    BandPhase.ValueEnd = end;
                }

                if (BandFreq != null)
                {
                    BandFreq.ValueBegin = begin;
                    BandFreq.ValueEnd = end;
                }
            }
            bandAmp = false;
        }

        private void BandPhase_ValuesChanged(object sender, ValuesChangedEventArgs e)
        {
            double begin = e.NewBeginValue;
            double width = e.NewEndValue - e.NewBeginValue;
            double end = e.NewEndValue;
            if (width < 0)
            {
                width *= (-1);
                begin = e.NewEndValue;
                end = e.NewBeginValue;
            }
            OnBandPhase?.Invoke(begin, width, end);

            tbLeft.Text = string.Format("{0:0.0}", begin);
            tbWidth.Text = string.Format("{0:0.0}", width);
            tbRight.Text = string.Format("{0:0.0}", end);

            bandPhase = true;
            if (bandAmp != true && bandFreq != true) 
            {
                if (BandAmp != null)
                {
                    BandAmp.ValueBegin = begin;
                    BandAmp.ValueEnd = end;
                }

                if (BandFreq != null)
                {
                    BandFreq.ValueBegin = begin;
                    BandFreq.ValueEnd = end;
                }
            }
            bandPhase = false;
        }

        private void BandFreq_ValuesChanged(object sender, ValuesChangedEventArgs e)
        {
            double begin = e.NewBeginValue;
            double width = e.NewEndValue - e.NewBeginValue;
            double end = e.NewEndValue;
            if (width < 0)
            {
                width *= (-1);
                begin = e.NewEndValue;
                end = e.NewBeginValue;
            }
            OnBandFreq?.Invoke(begin, width, end);

            tbLeft.Text = string.Format("{0:0.0}", begin);
            tbWidth.Text = string.Format("{0:0.0}", width);
            tbRight.Text = string.Format("{0:0.0}", end);

            bandFreq = true;
            if (bandAmp != true && bandPhase != true) 
            {
                if (BandAmp != null)
                {
                    BandAmp.ValueBegin = begin;
                    BandAmp.ValueEnd = end;
                }

                if (BandPhase != null)
                {
                    BandPhase.ValueBegin = begin;
                    BandPhase.ValueEnd = end;
                }
            }
            bandFreq = false;
        }
        #endregion

        #region GraphDoubleClick
        private void graphModule_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(graphModule);
            var view = graphModule.ViewXY;
            XAxAmp.CoordToValue((int)pos.X, out var xValue, true);

            BandAmp.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
            BandAmp.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
        }

        private void graphPhase_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(graphPhase);
            var view = graphPhase.ViewXY;
            XAxPhase.CoordToValue((int)pos.X, out var xValue, true);            

            BandPhase.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
            BandPhase.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
        }

        private void graphFreq_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(graphFreq);
            var view = graphFreq.ViewXY;
            XAxFreq.CoordToValue((int)pos.X, out var xValue, true);            

            BandFreq.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
            BandFreq.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
        }
        #endregion

        #region ViewXY_Zoomed
        private void ViewXY_Zoomed(object sender, ZoomedXYEventArgs e)
        {
            ViewXY view = graphModule.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            graphModule.BeginUpdate();
            graphModule.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphModule.EndUpdate();

            graphPhase.BeginUpdate();
            graphPhase.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphPhase.EndUpdate();

            graphFreq.BeginUpdate();
            graphFreq.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphFreq.EndUpdate();
        }

        private void ViewXYPhase_Zoomed(object sender, ZoomedXYEventArgs e) 
        {
            ViewXY view = graphPhase.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            graphModule.BeginUpdate();
            graphModule.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphModule.EndUpdate();

            graphPhase.BeginUpdate();
            graphPhase.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphPhase.EndUpdate();

            graphFreq.BeginUpdate();
            graphFreq.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphFreq.EndUpdate();
        }

        private void ViewXYFreq_Zoomed(object sender, ZoomedXYEventArgs e) 
        {
            ViewXY view = graphFreq.ViewXY;

            double currentMin = view.XAxes[0].Minimum;
            double currentMax = view.XAxes[0].Maximum;

            graphModule.BeginUpdate();
            graphModule.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphModule.EndUpdate();

            graphPhase.BeginUpdate();
            graphPhase.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphPhase.EndUpdate();

            graphFreq.BeginUpdate();
            graphFreq.ViewXY.XAxes[0].SetRange(currentMin, currentMax);
            graphFreq.EndUpdate();
        }
        #endregion
    }
}
