﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace DrawIQCounts
{
    public partial class DrawIQControl : UserControl
    {
        public delegate void SendCoordinatesFromGraphs(double xCursor, double yCursor);
        public event SendCoordinatesFromGraphs OnSendCoord = (xCursor, yCursor) => { };
        public event SendCoordinatesFromGraphs OnSendCoordPhase = (xCursor, yCursor) => { };
        public event SendCoordinatesFromGraphs OnSendCoordFreq = (xCursor, yCursor) => { };

        private const string Format = "{0:0}";
        Point posDownAmp;
        Point posUpAmp;
        Point posDownPhase;
        Point posUpPhase;
        Point posDownFreq;
        Point posUpFreq;
        double delay = 0.0145416259765625;

        private void FindItems(double xValue) 
        {
            int index = (int)Math.Round(xValue / delay);
            if (arrayAmp != null)
            {
                CursorY.Value = arrayAmp[index];
            }
            if (array != null)
            {
                CursorYPhase.Value = array[index];
            }
            if (arrayFrq != null)
            {
                CursorYFreq.Value = arrayFrq[index];
            }

            tbTime.Text = string.Format("{0:0.0}", xValue);
            tbAmp.Text = string.Format("{0:0.0}", CursorY.Value);
            tbPhase.Text = string.Format(Format, CursorYPhase.Value);
            tbFreq.Text = string.Format("{0:0.0}", CursorYFreq.Value);
        }

        private void graphModule_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDownAmp = e.GetPosition(graphModule);
        }

        private void graphModule_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var currPos = e.GetPosition(graphModule);
            var marginsRect = graphModule.ViewXY.GetMarginsRect();

            if ((currPos.X >= marginsRect.X) && (currPos.X <= marginsRect.X + marginsRect.Width) && (currPos.Y >= marginsRect.Y) && (currPos.Y <= marginsRect.Y + marginsRect.Height))
            {
                posUpAmp = e.GetPosition(graphModule);

                if (posUpDown(posUpAmp, posDownAmp))
                {
                    var position = e.GetPosition(graphModule);
                    XAxAmp.CoordToValue((int)position.X, out var xValue, true);
                    YAxAmp.CoordToValue((int)position.Y, out var yValue, true);

                    CursorX.ValueAtXAxis = xValue;                    
                    CursorXPhase.ValueAtXAxis = xValue;
                    CursorXFreq.ValueAtXAxis = xValue;
                    FindItems(xValue);
                    
                    OnSendCoord?.Invoke(xValue, yValue);
                }
            }
        }

        private void graphPhase__MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDownPhase = e.GetPosition(graphPhase);
        }

        private void praphPhase_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var currPos = e.GetPosition(graphPhase);
            var marginsRect = graphPhase.ViewXY.GetMarginsRect();

            if ((currPos.X >= marginsRect.X) && (currPos.X <= marginsRect.X + marginsRect.Width) && (currPos.Y >= marginsRect.Y) && (currPos.Y <= marginsRect.Y + marginsRect.Height))
            {
                posUpPhase = e.GetPosition(graphPhase);

                if (posUpDown(posUpPhase, posDownPhase))
                {
                    var position = e.GetPosition(graphPhase);
                    XAxPhase.CoordToValue((int)position.X, out var xValue, true);
                    YAxPhase.CoordToValue((int)position.Y, out var yValue, true);

                    CursorX.ValueAtXAxis = xValue;
                    CursorXPhase.ValueAtXAxis = xValue;
                    CursorXFreq.ValueAtXAxis = xValue;
                    FindItems(xValue);

                    OnSendCoordPhase?.Invoke(xValue, yValue);
                }
            }
        }

        private void graphFreq__MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            posDownFreq = e.GetPosition(graphFreq);
        }

        private void praphFreq_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var currPos = e.GetPosition(graphFreq);
            var marginsRect = graphFreq.ViewXY.GetMarginsRect();

            if ((currPos.X >= marginsRect.X) && (currPos.X <= marginsRect.X + marginsRect.Width) && (currPos.Y >= marginsRect.Y) && (currPos.Y <= marginsRect.Y + marginsRect.Height))
            {
                posUpFreq = e.GetPosition(graphFreq);

                if (posUpDown(posUpFreq, posDownFreq))
                {
                    var position = e.GetPosition(graphFreq);
                    XAxFreq.CoordToValue((int)position.X, out var xValue, true);
                    YAxFreq.CoordToValue((int)position.Y, out var yValue, true);

                    CursorX.ValueAtXAxis = xValue;
                    CursorXPhase.ValueAtXAxis = xValue;
                    CursorXFreq.ValueAtXAxis = xValue;
                    FindItems(xValue);

                    OnSendCoordFreq?.Invoke(xValue, yValue);
                }
            }
        }

        private bool posUpDown(Point posUp, Point posDown)
        {
            if (posDown == posUp)
                return true;

            if ((posDown.X - 1 <= posUp.X) && (posDown.X + 1 >= posUp.X) && (posDown.Y - 1 <= posUp.Y) && (posDown.Y + 1 >= posUp.Y))
                return true;
            else return false;
        }
    }
}
