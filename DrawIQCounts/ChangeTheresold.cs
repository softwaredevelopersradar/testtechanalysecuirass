﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DrawIQCounts
{
    public partial class DrawIQControl
    {
        public delegate void SendThresholdToCalculateDuration(double threshold);
        public event SendThresholdToCalculateDuration OnSendThreshold = (threshold) => { };

        double _threshold = 0.7;
        public double Threshold
        {
            get => _threshold;
            set
            {
                _threshold = value;
            }
        }

        private void ButThdUp_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tbThd.Text, out double thdUp);
            thdUp += 0.1;
            if (thdUp > 1.0) thdUp = 1.0;
            string thd = thdUp.ToString();
            if (thd.Length == 1) thd += ",0";
            Threshold = thdUp;

            tbThd.Text = null;
            tbThd.Text = thd.Substring(0, 1) + "," + thd.Substring(2, 1);
            CursorThd.Value = Threshold * maxAmplitude;

            OnSendThreshold?.Invoke(thdUp);
        }

        private void ButThdDown_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tbThd.Text, out double thdDown);
            thdDown -= 0.1;
            if (thdDown < 0.1) thdDown = 0.1;
            string thd = thdDown.ToString();
            Threshold = thdDown;

            tbThd.Text = null;
            tbThd.Text = thd.Substring(0, 1) + "," + thd.Substring(2, 1);
            CursorThd.Value = Threshold * maxAmplitude;

            OnSendThreshold?.Invoke(thdDown);
        }
    }
}
