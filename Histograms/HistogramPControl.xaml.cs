﻿using Arction.Wpf.SemibindableCharting;
using MathNet.Numerics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Histograms
{
    /// <summary>
    /// Логика взаимодействия для HistogramPControl.xaml
    /// </summary>
    public partial class HistogramPControl : UserControl
    {
        double max;
        double min;
        List<double> arrayDurations = new List<double>();
        List<string> stringDurations = new List<string>();

        string nameAx = "";
        public string NameAx
        {
            get => nameAx;
            set
            {
                nameAx = value;
                yTitle.Text = nameAx;
            }
        }

        double YLengthMin = 0;
        double YLengthMax = 5000;
        double XLengthMin = 0;
        double XLengthMax = 30;

        public HistogramPControl()
        {
            InitializeComponent();
        }

        public void DrawGraph(double[] duration)
        {
            DispatchIfNecessary(() =>
            {  
                var _view = GraphAmp.ViewXY;
                int count = duration.Length;
                arrayDurations = new List<double>();
                stringDurations = new List<string>();

                for (int i = 0; i < count; i++) 
                {
                    duration[i] = Math.Round(duration[i], 1);
                }
                /*
                double prev = duration[0];
                int num = 1;
                for (int i = 1; i < count; i++) 
                {
                    if (duration[i] == prev) 
                    {
                        num++;
                        prev = duration[i];

                        if (i == count - 1) 
                        {
                            arrayDurations.Add(num);
                            stringDurations.Add(duration[i].ToString());
                        }
                    }
                    else 
                    {
                        arrayDurations.Add(num);
                        stringDurations.Add(prev.ToString());
                        num = 1;
                        prev = duration[i];

                        if (i == count - 1) 
                        {
                            arrayDurations.Add(num);
                            stringDurations.Add(duration[i].ToString());
                        }
                    }
                }
                */
                var countThis = duration.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());
                int countfProc = countThis.ToArray().Length;
                BarSeriesValue[] barSeries = new BarSeriesValue[countfProc];

                //for (int i = 0; i < countfProc; i++) 
                //{
                //    barSeries[i].Location = i + 1;
                //    barSeries[i].Value = (arrayDurations[i] * 100) / count;
                //    barSeries[i].Text = string.Format("{0:0.0}", barSeries[i].Value) + "% " + stringDurations[i].ToString() + " us";            //stringDurations        
                //}

                byte ind = 0;
                foreach (var (keyEl, valEl) in from elements in countThis
                                               let keyEl = elements.Key
                                               let valEl = elements.Value
                                               select (keyEl, valEl))
                {
                    barSeries[ind].Location = ind + 1;
                    barSeries[ind].Value = (valEl * 100f) / count;
                    barSeries[ind].Text = string.Format("{0:0.0}", barSeries[ind].Value) + "% " + keyEl.ToString() + " us";
                    ind++;
                }

                #region Test
                //var data = new SeriesPoint[duration.Length];
                /*
                double[] doubleData = new double[count * 2];                
                yValues = new double[count];
                int ii = 0;
                for (int i = 0; i < count; i++)
                {                    
                    for (int j = 0; j < 2; j++)
                    {
                        doubleData[ii] = duration[i];
                        ii++;
                    }                    
                }
                var data = new SeriesPoint[doubleData.Length];
                var last = doubleData[0];
                int step = 0;
                int lastStep = 1;
                for (int i = 0; i < doubleData.Length; i++)
                {
                    //barSeries[i].Location = i + 1;
                    //barSeries[i].Value = duration[i];
                    
                    if (doubleData[i] == last)
                    {
                        if (step == lastStep)
                        {
                            step++;
                            data[i].X = step;
                            data[i].Y = doubleData[i];
                        }
                        else
                        {
                            data[i].X = step;
                            data[i].Y = doubleData[i];
                        }                        
                        step++;
                        lastStep = step + 1;
                    }
                    else
                    {
                        last = doubleData[i];
                        step--;

                        data[i].X = step;
                        data[i].Y = doubleData[i];

                        lastStep = step;
                    }

                    //yValues[i] = barSeries[i].Value;
                }
                */
                #endregion

                GraphAmp.BeginUpdate();

                YLengthMin = 0;
                YLengthMax = 105;
                XLengthMin = 0;
                XLengthMax = countfProc * 2;

                XAxAmp.Minimum = 0;
                XAxAmp.Maximum = countfProc * 2;
                YAxAmp.Minimum = 0;
                YAxAmp.Maximum = 105;
                _view.BarSeries[0].Values = barSeries;
                
                GraphAmp.EndUpdate();                
            });
        }

        private void YAxAmp_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = YAxAmp;
                if (e.NewMax - e.NewMin > YLengthMax - YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                    return;
                }
                if (e.NewMax > YLengthMax)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
            }
            catch { }
        }

        private void XAxAmp_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = XAxAmp;

                if (e.NewMax - e.NewMin > XLengthMax - XLengthMin)
                {
                    aXysX.SetRange(XLengthMin, XLengthMax);
                    return;
                }
                if (e.NewMin < XLengthMin)
                {
                    double shift = XLengthMin - e.NewMin;
                    aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
                }
                if (e.NewMax > XLengthMax)
                {
                    double shift = e.NewMax - XLengthMax;
                    aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
                }
            }
            catch { }
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
    }
}
