﻿using System.Windows.Controls;

namespace ResultsFromGraphы
{
    /// <summary>
    /// Логика взаимодействия для ResultsGraphControl.xaml
    /// </summary>
    public partial class ResultsGraphControl : UserControl
    {
        public ResultsGraphControl()
        {
            InitializeComponent();
        }

        string yname = "Level";
        public string YName
        {
            get => yname;
            set
            {
                yname = value;
                lYName.Text = yname;
            }
        }

        string lname = "";
        public string LData
        {
            get => lname;
            set
            {
                lname = value;
                lLevel.Text = lname;
            }
        }

        string ltime = "";
        public string LTime
        {
            get => ltime;
            set
            {
                ltime = value;
                lTime.Text = ltime;
            }
        }

        string leftD = "";
        public string LeftD
        {
            get => leftD;
            set
            {
                leftD = value;
                lLeftD.Text = leftD;
            }
        }

        string widthD = "";
        public string WidthD
        {
            get => widthD;
            set
            {
                widthD = value;
                lWidthD.Text = widthD;
            }
        }

        string rightD = "";
        public string RightD
        {
            get => rightD;
            set
            {
                rightD = value;
                lRightD.Text = rightD;
            }
        }
    }
}
