﻿using Arction.Wpf.SemibindableCharting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DrawPartIQCounts
{
    /// <summary>
    /// Логика взаимодействия для DrawPartIQControl.xaml
    /// </summary>
    public partial class DrawPartIQControl : UserControl
    {
        string nameAxP = "";
        public string NameAxP
        {
            get => nameAxP;
            set
            {
                nameAxP = value;
                yPTitle.Text = nameAxP;
            }
        }

        double max;
        double min;

        double YLengthMin = 0;
        double YLengthMax = 5000;
        double XLengthMin = 0;
        double XLengthMax = 30;

        double YFrqLengthMin = -50;
        double YFrqLengthMax = 50;
        double XFrqLengthMin = 0;
        double XFrqLengthMax = 10;

        double delay = 0.0145416259765625;

        public DrawPartIQControl()
        {
            InitializeComponent();
        }

        public void DrawAmp(double[] amplitudeArray, double bLeft, double bWidth, double bRight)
        {
            DispatchIfNecessary(() =>
            {
                try
                {
                    var _view = GraphAmp.ViewXY;
                    FindMax(amplitudeArray);

                    if (bLeft < 0)
                    {
                        bLeft = 0;
                        bWidth = bRight;
                    }

                    int index = (int)Math.Round(bLeft / delay);
                    int indW = (int)Math.Round(bWidth / delay);
                    if (index < amplitudeArray.Length) 
                    {
                        var data = new SeriesPoint[indW];
                        int ind = 0;

                        for (int i = index; i < index + indW; i++)
                        {
                            data[ind].X = delay * i;
                            data[ind].Y = amplitudeArray[i];
                            ind++;
                        }

                        GraphAmp.BeginUpdate();

                        XLengthMin = bLeft;
                        XLengthMax = bRight;
                        YLengthMin = 0;
                        YLengthMax = max + 250;

                        XAxAmp.Minimum = bLeft;
                        XAxAmp.Maximum = bRight;
                        YAxAmp.Minimum = 0;
                        YAxAmp.Maximum = max + 250;
                        _view.PointLineSeries[0].Points = data;

                        GraphAmp.EndUpdate();
                    }                    
                }
                catch { }                
            });
        }

        public void DrawPhase(double[] phaseArray, double bLeft, double bWidth, double bRight)
        {
            DispatchIfNecessary(() => 
            {
                try
                {
                    var _view = GraphPhase.ViewXY;
                    FindMax(phaseArray);
                    FindMin(phaseArray);

                    if (bLeft < 0)
                    {
                        bLeft = 0;
                        bWidth = bRight;
                    }

                    int index = (int)Math.Round(bLeft / delay);
                    int indW = (int)Math.Round(bWidth / delay);

                    if (index < phaseArray.Length) 
                    {
                        var data = new SeriesPoint[indW];
                        int ind = 0;

                        for (int i = index; i < index + indW; i++)
                        {
                            data[ind].X = delay * i;
                            data[ind].Y = phaseArray[i];
                            ind++;
                        }

                        GraphPhase.BeginUpdate();

                        //XPhaseLengthMin = 0;
                        //XPhaseLengthMax = delay * data.Length;
                        //YPhaseLengthMin = min - 20;
                        //YPhaseLengthMax = max + 20;

                        XAxPhase.Minimum = bLeft;
                        XAxPhase.Maximum = bRight;
                        YAxPhase.Minimum = min - 20;
                        YAxPhase.Maximum = max + 20;
                        _view.PointLineSeries[0].Points = data;

                        GraphPhase.EndUpdate();
                    }                    
                }
                catch { }                
            });
        }

        public void DrawFreq(double[] freqArray, double bLeft, double bWidth, double bRight)
        {
            DispatchIfNecessary(() => 
            {
                try
                {                    
                    var _view = GraphFreq.ViewXY;                    
                    FindMin(freqArray);
                    FindMax(freqArray);

                    if (bLeft < 0)
                    {
                        bLeft = 0;
                        bWidth = bRight;
                    }

                    int index = (int)Math.Round(bLeft / delay);
                    int indW = (int)Math.Round(bWidth / delay);

                    if (index < freqArray.Length) 
                    {
                        var data = new SeriesPoint[indW];
                        int ind = 0;

                        for (int i = index; i < index + indW; i++)
                        {
                            data[ind].X = delay * i;
                            data[ind].Y = freqArray[i];
                            ind++;
                        }

                        GraphFreq.BeginUpdate();

                        XFrqLengthMin = bLeft;
                        XFrqLengthMax = bRight;

                        YAxFreq.Minimum = YFrqLengthMin;
                        YAxFreq.Maximum = YFrqLengthMax;
                        XAxFreq.Minimum = bLeft;
                        XAxFreq.Maximum = bRight;
                        _view.PointLineSeries[0].Points = data;

                        GraphFreq.EndUpdate();
                    }                    
                }
                catch { }
            });
        }
        
        private void FindMax(double[] array)
        {
            max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (max < array[i])
                {
                    max = array[i];
                }
            }
        }

        private void FindMin(double[] array)
        {
            min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                }
            }
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
    }
}
