﻿using Arction.Wpf.SemibindableCharting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace DrawPartIQCounts
{
    public partial class DrawPartIQControl : UserControl
    {
        bool bandAmp = false;
        bool bandPhase = false;
        bool bandFreq = false;

        #region AxRangeChanged
        private void YAxAmp_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysY = YAxAmp;
                if (e.NewMax - e.NewMin > YLengthMax - YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                    return;
                }
                if (e.NewMax > YLengthMax)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
                if (e.NewMin < YLengthMin)
                {
                    aXysY.SetRange(YLengthMin, YLengthMax);
                }
            }
            catch { }
        }

        private void YAxFreq_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = YAxFreq;
                if (e.NewMax - e.NewMin > YFrqLengthMax - YFrqLengthMin)
                {
                    aXysX.SetRange(YFrqLengthMin, YFrqLengthMax);
                    return;
                }
                if (e.NewMin < YFrqLengthMin)
                {
                    double shift = YFrqLengthMin - e.NewMin;
                    if(e.NewMax == YFrqLengthMax) 
                    {
                        aXysX.SetRange(e.NewMin + shift, e.NewMax);
                    }
                    else { aXysX.SetRange(e.NewMin + shift, e.NewMax + shift); }
                }
                if (e.NewMax > YFrqLengthMax)
                {
                    double shift = e.NewMax - YFrqLengthMax;
                    if (e.NewMin == YFrqLengthMin) 
                    {
                        aXysX.SetRange(e.NewMin - shift, e.NewMax);
                    }
                    else { aXysX.SetRange(e.NewMin - shift, e.NewMax - shift); }
                    
                }
            }
            catch { }
        }

        private void XAxFreq_RangeChanged(object sender, Arction.Wpf.SemibindableCharting.Axes.RangeChangedEventArgs e)
        {
            try
            {
                var aXysX = XAxFreq;
                if (e.NewMax - e.NewMin > XFrqLengthMax - XFrqLengthMin)
                {
                    aXysX.SetRange(XFrqLengthMin, XFrqLengthMax);
                    return;
                }
                if (e.NewMin < XFrqLengthMin)
                {
                    double shift = XFrqLengthMin - e.NewMin;

                    aXysX.SetRange(e.NewMin + shift, e.NewMax + shift);
                }
                if (e.NewMax > XFrqLengthMax)
                {
                    double shift = e.NewMax - XFrqLengthMax;
                    aXysX.SetRange(e.NewMin - shift, e.NewMax - shift);
                }
            }
            catch { }
        }
        #endregion

        #region Bands
        private void BandAmp_ValuesChanged(object sender, ValuesChangedEventArgs e)
        {
            double begin = e.NewBeginValue;
            double width = e.NewEndValue - e.NewBeginValue;
            double end = e.NewEndValue;
            if (width < 0)
            {
                width *= (-1);
                begin = e.NewEndValue;
                end = e.NewBeginValue;
            }

            tbLeft.Text = string.Format("{0:0.0}", begin);
            tbWidth.Text = string.Format("{0:0.0}", width);
            tbRight.Text = string.Format("{0:0.0}", end);

            bandAmp = true;
            if (bandPhase != true && bandFreq != true)
            {
                if (BandPhase != null)
                {
                    BandPhase.ValueBegin = begin;
                    BandPhase.ValueEnd = end;
                }

                if (BandFreq != null)
                {
                    BandFreq.ValueBegin = begin;
                    BandFreq.ValueEnd = end;
                }
            }
            bandAmp = false;
        }

        private void BandPhase_ValuesChanged(object sender, ValuesChangedEventArgs e)
        {
            double begin = e.NewBeginValue;
            double width = e.NewEndValue - e.NewBeginValue;
            double end = e.NewEndValue;
            if (width < 0)
            {
                width *= (-1);
                begin = e.NewEndValue;
                end = e.NewBeginValue;
            }            

            tbLeft.Text = string.Format("{0:0.0}", begin);
            tbWidth.Text = string.Format("{0:0.0}", width);
            tbRight.Text = string.Format("{0:0.0}", end);

            bandPhase = true;
            if (bandAmp != true && bandFreq != true)
            {
                if (BandAmp != null)
                {
                    BandAmp.ValueBegin = begin;
                    BandAmp.ValueEnd = end;
                }

                if (BandFreq != null)
                {
                    BandFreq.ValueBegin = begin;
                    BandFreq.ValueEnd = end;
                }
            }
            bandPhase = false;
        }

        private void BandFreq_ValuesChanged(object sender, ValuesChangedEventArgs e)
        {
            double begin = e.NewBeginValue;
            double width = e.NewEndValue - e.NewBeginValue;
            double end = e.NewEndValue;
            if (width < 0)
            {
                width *= (-1);
                begin = e.NewEndValue;
                end = e.NewBeginValue;
            }

            tbLeft.Text = string.Format("{0:0.0}", begin);
            tbWidth.Text = string.Format("{0:0.0}", width);
            tbRight.Text = string.Format("{0:0.0}", end);

            bandFreq = true;
            if (bandAmp != true && bandPhase != true)
            {
                if (BandAmp != null)
                {
                    BandAmp.ValueBegin = begin;
                    BandAmp.ValueEnd = end;
                }

                if (BandPhase != null)
                {
                    BandPhase.ValueBegin = begin;
                    BandPhase.ValueEnd = end;
                }
            }
            bandFreq = false;
        }
        #endregion

        #region GraphDoubleClick
        private void graphModule_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(GraphAmp);
            var view = GraphAmp.ViewXY;
            XAxAmp.CoordToValue((int)pos.X, out var xValue, true);

            BandAmp.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
            BandAmp.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
        }

        private void graphPhase_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(GraphPhase);
            var view = GraphPhase.ViewXY;
            XAxPhase.CoordToValue((int)pos.X, out var xValue, true);

            BandPhase.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
            BandPhase.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
        }

        private void graphFreq_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var pos = e.GetPosition(GraphFreq);
            var view = GraphFreq.ViewXY;
            XAxFreq.CoordToValue((int)pos.X, out var xValue, true);

            BandFreq.ValueBegin = xValue - (xValue - view.XAxes[0].Minimum) / 4f;
            BandFreq.ValueEnd = xValue + (view.XAxes[0].Maximum - xValue) / 4f;
        }
        #endregion
    }
}
