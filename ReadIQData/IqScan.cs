﻿using System;

namespace DspDataModel.Data
{
    public class IqScan
    {
        public short Real { get; private set; }

        public short Imaginary { get; private set; }

        public IqScan()
        {
        }

        public IqScan(short real, short imaginary)
        {
            this.Real = real;
            this.Imaginary = imaginary;
        }

        public static IqScan[] IqBytesToScan(byte[] iqBytes, int scanSize = 256)
        {
            var iqScans = new IqScan[scanSize];
            if (iqBytes.Length != 1)
            {
                for (int i = 0; i < scanSize; i++)
                {
                    var realBytes = new byte[] { iqBytes[4 * i + 1], iqBytes[4 * i] };
                    var imgBytes = new byte[] { iqBytes[4 * i + 3], iqBytes[4 * i + 2] };
                    var real = BitConverter.ToInt16(realBytes, 0);
                    var imaginary = BitConverter.ToInt16(imgBytes, 0);
                    iqScans[i] = new IqScan(real, imaginary);
                }
            }

            return iqScans;
        }
    }


    public static class IqExtensions
    {
        public static string DoubleArrayToString(this double[] array)
        {
            var output = "";
            for (int i = 0; i < array.Length; i++)
            {
                output += $"{array[i]} ";
            }

            return output;
        }

        public static string ShortArrayToString(this short[] array)
        {
            var output = "";
            for (int i = 0; i < array.Length; i++)
            {
                output += $"{array[i]} ";
            }

            return output;
        }

        public static string IntArrayToString(this int[] array)
        {
            var output = "";
            for (int i = 0; i < array.Length; i++)
            {
                output += $"{array[i]} ";
            }

            return output;
        }
    }
}
