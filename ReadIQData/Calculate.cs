﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;

using MathNet.Numerics.IntegralTransforms;

namespace ReadIQData
{
    public class Calculate
    {
        #region PropertyCalculateAmp
        bool key = false;   // true - Fedya, false - Ftm

        List<double> countRe = new List<double>();
        List<double> countIm = new List<double>();

        List<double> bigList = new List<double>();
        List<double> bigListIm = new List<double>();

        readonly bool isPaste = false;
        readonly int numPaste = 100;

        public List<double> phaseArray = new List<double>();
        public List<double> amplitudeArray = new List<double>();
        public List<double> freqArray = new List<double>();

        public List<double> spArray = new List<double>();
        public List<double> spArrayDb = new List<double>();

        Complex complexPoint;
        Complex[] complexArray;
        public double[] arrayFFT;

        double f_imp = 74.7681;
        public double F_IMP
        {
            get => f_imp;
            set { f_imp = value; }
        }

        double Fs = 68.7681;  //MHz

        public event EventHandler OnCalculate = (sender , obj) => { };
        public event EventHandler OnCalculatePhase = (sender, obj) => { };
        public event EventHandler OnCalculateFreq = (sender, obj) => { };

        public delegate void SendCalculatedPSK(double[] array);
        public event SendCalculatedPSK OnCalculatedPSK = (array) => { };

        public delegate void SendSpectrogramSize(int length);
        public event SendSpectrogramSize OnSpectrogramSize = (length) => { };

        public delegate void SendExistFilePanth(bool Exists);
        public event SendExistFilePanth OnSendExistsFilePathRe = (exists) => { };
        public event SendExistFilePanth OnSendExistsFilePathIm = (exists) => { };

        public delegate void SendFreqStatistics(double min, double max, double average);
        public event SendFreqStatistics OnSendFreqStat = (min, max, average) => { };

        public enum Modulation
        {
            AM = 0,
            FSK = 1,
            PSK = 2
        }
        #endregion

        #region PropertyCalculateDuration
        double max;
        double min;
        double average;
        double delay = 0.0145416259765625; // 0.0146484375
        List<double> arrayMax;
        List<double> arrayDel;
        List<List<double>> stackArrays;
        List<List<double>> stackDuration;
        double lowLevel;
        double highLevel;
        double firstLevel;
        List<double> sort;
        List<double> sortD1;
        List<double> sortD2;

        List<double> lArrayDur;
        List<double> lArrayPeriod;
        List<int> arrayMinMaxElements;
        List<List<int>> lArraysMiMax;

        double _threshold = 0.7;
        public double Threshold
        {
            get => _threshold;
            set
            {
                _threshold = value;
                CalculateDuration();
            }
        }

        public delegate void SendDuration(double[] duration, double[] period);
        public event SendDuration OnSendDuration;
        #endregion

        #region ReadData

        public void ReadDataBoth(string path)
        {
            try
            {
                this.bigList.Clear();
                this.bigListIm.Clear();
                this.countRe.Clear();
                this.countIm.Clear();

                using (var fs = File.Open(path, FileMode.Open))
                using (var sr = new StreamReader(fs))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        var splited = s.Split(' ');
                        if (splited.Length == 2)
                        {
                            this.countRe.Add(Convert.ToInt16(splited[0]));
                            this.countIm.Add(Convert.ToInt16(splited[1]));
                        }
                    }
                }

                if (this.isPaste)
                {
                    for (int i = 0; i < this.numPaste; i++)
                    {
                        this.bigList.AddRange(this.countRe);
                        this.bigListIm.AddRange(this.bigListIm);
                    }
                    this.countRe = new List<double>(this.bigList);
                    this.countIm = new List<double>(this.bigListIm);
                }

                this.OnSendExistsFilePathRe?.Invoke(true);
                this.OnSendExistsFilePathIm?.Invoke(true);

            }
            catch 
            {
                // ignored
            }
        }   

        public void ReadDataRe(string Path)
        {
            try
            {
                if (File.Exists(Path))
                {
                    if (this.key)
                    {
                        this.bigList.Clear();
                        this.countRe.Clear();
                        var separator = ' ';
                        using (var fs = File.Open(Path, FileMode.Open))
                        using (var bs = new BufferedStream(fs))
                        using (var sr = new StreamReader(bs))
                        {
                            string s;
                            while ((s = sr.ReadLine()) != null)
                            {

                                if (s.IndexOf(separator) != -1)
                                {
                                    this.countRe = s.Split(separator)
                                        .Select(Convert.ToDouble)
                                        .ToList();
                                }
                                else
                                {
                                    this.countRe.Add(Convert.ToDouble(s));
                                }
                            }
                        }

                        if (this.isPaste)
                        {
                            for (var i = 0; i < this.numPaste; i++)
                            {
                                this.bigList.AddRange(this.countRe);
                            }

                            this.countRe = new List<double>(this.bigList);
                        }
                    }
                    else
                    {
                        var allLines = File.ReadAllLines(Path).ToList();
                        this.countRe = allLines.AsParallel().Select(Convert.ToDouble).ToList();
                    }

                    this.OnSendExistsFilePathRe?.Invoke(true);
                }
                else
                {
                    this.OnSendExistsFilePathRe?.Invoke(false);
                }
            }
            catch
            {
                // ignored
            }
        }

        public void ReadDataIm(string Path)
        {
            try
            {
                if (File.Exists(Path))
                {
                    if (this.key)
                    {
                        this.bigListIm.Clear();
                        this.countIm.Clear();
                        var separator = ' ';
                        using (FileStream fs = File.Open(Path, FileMode.Open))
                        using (BufferedStream bs = new BufferedStream(fs))
                        using (StreamReader sr = new StreamReader(bs))
                        {
                            string s;
                            while ((s = sr.ReadLine()) != null)
                            {

                                if (s.IndexOf(separator) != -1)
                                {
                                    this.countIm = s.Split(separator)
                                        .Select(Convert.ToDouble)
                                        .ToList();
                                }
                                else
                                {
                                    this.countIm.Add(Convert.ToDouble(s));
                                }
                            }
                        }

                        if (this.isPaste)
                        {
                            for (var i = 0; i < this.numPaste; i++)
                            {
                                this.bigListIm?.AddRange(this.countIm);
                            }

                            this.countIm = new List<double>(this.bigListIm);
                        }

                    }
                    else
                    {
                        var allLines = File.ReadAllLines(Path).ToList();
                        this.countIm = allLines.AsParallel().Select(Convert.ToDouble).ToList();
                    }
                    this.OnSendExistsFilePathIm?.Invoke(true);
                }
                else
                {
                    this.OnSendExistsFilePathIm?.Invoke(false);
                }
            }
            catch { }
        }

        #endregion

        #region Calculate Amp Phase Freq SP FFT

        public void CalculatePhase(Modulation modulation)
        {
            try
            {
                switch (modulation)
                {
                    case Modulation.AM:
                        this.CalculatePhaseAM();
                        break;
                    case Modulation.PSK:
                        this.CalculatePSK();
                        break;
                    default:
                        this.CalculatePhaseAM();
                        break;
                }

                this.OnCalculatePhase?.Invoke(this, null);
            }
            catch
            {
                // ignored
            }
        }

        private void CalculatePhaseAM() 
        {
            try
            {
                this.phaseArray.Clear();
                for (var i = 0; i < this.countIm.Count; i++)
                {
                    // todo think about creating a single list of type complex.
                    var phase = Math.Atan2(this.countIm[i], this.countRe[i]);
                    var phaseDeg = phase * (180 / Math.PI);
                    this.phaseArray.Add(phaseDeg);
                }
            }
            catch
            {
                // ignored
            }
        }

        private void CalculatePSK() 
        {
            /*
            var signalGet_1 = Operations.FormAGeterodyne("signal_I", countRe.Length);
            var signalGet_2 = Operations.FormAGeterodyne("signal_Q", countRe.Length);

            var signalDouble_1 = Operations.FormADoubleSignal(signalGet_1);
            var signalDouble_2 = Operations.FormADoubleSignal(signalGet_2);

            var signalI_1 = Operations.FormASignal(countRe, signalGet_1, signalDouble_1);
            var signalQ_1 = Operations.FormASignal(countIm, signalGet_1, signalDouble_1);
            var signalI_2 = Operations.FormASignal(countRe, signalGet_2, signalDouble_2);
            var signalQ_2 = Operations.FormASignal(countIm, signalGet_2, signalDouble_2);

            var signalFT_1 = Operations.FormFTSignal(signalI_1, signalQ_1);
            var signalFT_2 = Operations.FormFTSignal(signalI_2, signalQ_2);
            */
            var lastMemory = 0;
            var isOne = false;
            var isLast = false;
            var lastA = 0;

            foreach (var index in this.lArraysMiMax) 
            {
                lastA++;
                if (lastA == this.lArraysMiMax.ToArray().Length)
                {
                    isLast = true;
                }

                // todo Think about how convert FormComplex method to list.
                var signalComplex = Operations.FormComplex(this.countRe.ToArray(), this.countIm.ToArray(), index[0], index[1]);
                var signalPhaze = Operations.FormAGeterodyne("signal_2", this.countRe.Count);
                var s_g = Operations.ComplexMultiple(signalPhaze, signalComplex);

                var fft = Operations.CalculateFft(s_g);

                for (var i = 0; i < fft.Length; i++)
                {
                    fft[i] = i > 200000 || i < 80 ? fft[i] * 1 : fft[i] * 0;
                }

                var sGi = Operations.CalculateInverseFft(fft);

                var firstArray = this.lArraysMiMax[0];
                var secondfArray = this.lArraysMiMax[1];
                var zeros = secondfArray[0] - firstArray[1] - 1;

                var countZero = 0;
                countZero = index[0] == 0 ? 0 : index[0] - lastMemory + 1;

                if (countZero == 0) 
                {
                    for (var j = 0; j <= index[1]; j++) 
                    {
                        this.phaseArray.Add(sGi[j].Real);
                    }

                    for (var i = 0; i < zeros; i++) 
                    {
                        this.phaseArray.Add(0);
                    }

                    isOne = true;
                }
                else 
                {
                    if (!isOne)
                    {
                        for (var i = 0; i < countZero; i++)
                        {
                            this.phaseArray.Add(0);
                        }
                    }

                    isOne = false;
                    foreach (var t in sGi)
                    {
                        this.phaseArray.Add(t.Real);
                    }

                    if (isLast)
                    {
                        for (var i = 0; i <= this.countRe.Count - index[1]; i++)
                        {
                            this.phaseArray.Add(0);
                        }
                    }
                }

                lastMemory = index[1];              
            }
            // OnCalculatedPSK?.Invoke(amplitudeArray);            
        }

        private void CalculateAM() 
        {
            this.amplitudeArray.Clear();
            for (var i = 0; i < this.countIm.Count; i++)
            {
                var amp = Math.Sqrt(Math.Pow(this.countRe[i], 2) + Math.Pow(this.countIm[i], 2));  // complex envelope
                var amplitude = Math.Abs(amp);  // physical envelope
                this.amplitudeArray.Add(amplitude);
            }
        }

        public void CalculateModulAmplitude(Modulation modulation)
        {
            try
            { 
                switch (modulation) 
                {
                    case Modulation.AM:
                        this.CalculateAM();
                        break;
                    case Modulation.PSK:
                        this.CalculatePSK();
                        break;
                    default:
                        this.CalculateAM();
                        break;
                }

                OnCalculate?.Invoke(this, null);
            }
            catch { }
        }

        public void CalculateFreq(Modulation modulation)
        {
            try
            {
                switch (modulation) 
                {
                    case Modulation.AM:
                        this.freqArray.Clear();
                        for (var i = 0; i < this.countIm.Count; i++)
                        {
                            var der = (this.countRe[i] - this.countIm[i]) / (Math.Pow(this.countRe[i], 2) + Math.Pow(this.countIm[i], 2)); // derivative function                     
                            var freq = der * (2 * Math.PI);
                            this.freqArray.Add(freq);
                        }

                        break;
                    case Modulation.FSK:
                        this.freqArray.Clear();
                        var I1 = Operations.FormAnArray("firstZero", this.countRe);
                        var I2 = Operations.FormAnArray("lastZero", this.countRe);
                        var Q1 = Operations.FormAnArray("firstZero", this.countIm);
                        var Q2 = Operations.FormAnArray("lastZero", this.countIm);

                        var detect = new double[1, this.countRe.Count + 1];
                                                
                        var coef = -0.365 * (Math.Abs((F_IMP - Fs) * 1e6) / 10e6) + 1.265;

                        for (var i = 0; i < this.countRe.Count + 1; i++)
                        {
                            var diff = (I1[0, i] * Q2[0, i]) - (Q1[0, i] * I2[0, i]);

                            //var sum = Math.Pow(Math.Pow(I1[0, i], 2) + Math.Pow(Q1[0, i], 2), 0.8);  
                            var _sum = (coef * Math.Pow(Math.Pow(I1[0, i], 2) + Math.Pow(Q1[0, i], 2), 1));
                            detect[0, i] = (diff / _sum) * (Fs / 6);  // detect[0, i] = diff / (98.6 * sum) * 48.768;
                        }

                        for (var i = 0; i < this.countRe.Count; i++)
                        {
                            this.freqArray.Add(detect[0, i + 1]);
                        }

                        break;
                    case Modulation.PSK:
                        break;
                }

                this.OnCalculateFreq?.Invoke(this, null);
                CalculateFreqStat(this.freqArray);
            }
            catch { }
        }

        public void CalculateFreqStat(List<double> FreqArray) 
        {
            // for (int i = 0; i < FreqArray.Length; i++) 
            // {
            // FreqArray[i] = Math.Abs(FreqArray[i]);
            // }
            this.min = FreqArray.Min();
            this.max = FreqArray.Max();
            this.average = FreqArray.Average();
            this.OnSendFreqStat?.Invoke(this.min, this.max, this.average);
        }

        public void CalculateFFT()
        {
            int count = this.countIm.Count;
            this.complexArray = new Complex[count];
            this.arrayFFT = new double[count];
            for (var i = 0; i < count / 10; i++)
            {
                this.complexPoint = new Complex(this.countRe[i], this.countIm[i]);
                this.complexArray[i] = this.complexPoint;
            }

            Fourier.Forward(this.complexArray, FourierOptions.NoScaling);
            for (var i = 0; i < count / 10; i++)
            {
                var mag = Math.Abs(
                    Math.Sqrt(Math.Pow(this.complexArray[i].Real, 2) + Math.Pow(this.complexArray[i].Imaginary, 2)));
                this.arrayFFT[i] = 20 * Math.Log10(mag);

                // arrayFFT[i] = complexArray[i].Real;
            }
        }

        public void CalculateSP()
        {
            this.spArray.Clear();
            this.spArrayDb.Clear();

            for (var i = 0; i < this.countRe.Count; i++)
            {
                this.spArray.Add(Math.Pow(this.countRe[i], 2) + Math.Pow(this.countIm[i], 2));
                this.spArrayDb.Add(10 * Math.Log10(this.spArray[i]));
            }

            this.OnSpectrogramSize?.Invoke(this.spArrayDb.Count);
            this.OnCalculate?.Invoke(this, null);
        }

        #endregion

        #region Test Some Ideas
        private void Test()
        {
            /*
for (int i = 0; i < countRe.Length; i++)
{
    complex = new System.Numerics.Complex(countRe[i], countIm[i]);
    compValue[i] = complex;
}

Fourier.Inverse(compValue, FourierOptions.Matlab);
//Fourier.NaiveInverse(countRe, countIm, FourierOptions.Matlab);
int k = 3500;
int N = 15000;
for (int i = 0; i < k; i++)
{
    qwe = (System.Numerics.Complex.Abs(compValue[i]) * 1000000 * 100) / (N * 115 * 2400);
    //qwe = 20 * Math.Log10(qwe);
    spArray[i] = qwe;
}
*/

            /*
spec_data = new List<List<double>>();
double[] data = new double[fft_size];
List<double> data_empty = new List<double>();
data_empty.AddRange(countRe);
data_empty.GetRange(0, fft_size).ToArray();

spec_data.Add(data_empty);
//data = unanalyzed_values.GetRange(0, fft_size).ToArray();

//NAudio.Dsp.Complex[] complex = new NAudio.Dsp.Complex[countRe.Length];
//int fftSize = countRe.Length;
//Complex[] complices = new Complex[countRe.Length];

// remove the left-most (oldest) column of data

//spec_data.RemoveAt(0);

// insert new data to the right-most (newest) position
List<double> new_data = new List<double>();


// prepare the complex data which will be FFT'd
NAudio.Dsp.Complex[] fft_buffer = new NAudio.Dsp.Complex[fft_size];
for (int i = 0; i < fft_size; i++)
{
    //fft_buffer[i].X = (float)unanalyzed_values[i]; // no window
    fft_buffer[i].X = (float)(countRe[i] * FastFourierTransform.HammingWindow(i, fft_size));
    fft_buffer[i].Y = 0;
}

// perform the FFT
FastFourierTransform.FFT(true, (int)Math.Log(fft_size, 2.0), fft_buffer);

// fill that new data list with fft values
for (int i = 0; i < spec_data[spec_data.Count - 1].Count; i++)
{
    // should this be sqrt(X^2+Y^2)?
    double val;
    val = (double)fft_buffer[i].X + (double)fft_buffer[i].Y;
    val = Math.Abs(val);

    new_data.Add(val);
}

new_data.Reverse();
spec_data.Insert(spec_data.Count, new_data); // replaces, doesn't append!

for (int i = 0; i < spec_data.Count; i++)
{
    spArray[i] = fftb[i];
}
*/
            // Было на 13,04
            /*
            for (int i = 0; i < countRe.Length; i++)
            {
                complex[i].X = (float)countRe[i];
                complex[i].Y = (float)countIm[i];
                complex[i].X *= (float)NAudio.Dsp.FastFourierTransform.HammingWindow(i, fftSize);
                complex[i].Y *= (float)NAudio.Dsp.FastFourierTransform.HammingWindow(i, fftSize);

                //float pow = (float)Math.Sqrt(Math.Pow(complex[i].X, 2) + Math.Pow(complex[i].Y, 2));
                //spArray[i] = pow;
                //complices[i] = complex[i];

            }
            
            //Fourier.Inverse(complex, FourierOptions.Matlab);
            NAudio.Dsp.FastFourierTransform.FFT(false, (int)Math.Log(fftSize, 2.0), complex);

            for (int i = 0; i < countRe.Length-1; i++)
            {
                //spArrayRe[i] = countRe[i] * (float)NAudio.Dsp.FastFourierTransform.HammingWindow(i, countRe.Length);
                //spArrayIm[i] = countIm[i] * (float)NAudio.Dsp.FastFourierTransform.HammingWindow(i, countIm.Length);
                //double sp = Math.Pow(spArrayRe[i], 2) + Math.Pow(spArrayIm[i], 2);

                var sp1 = complex[i];
                //float abs = (float)Math.Sqrt(Math.Pow(sp1.X, 2) + Math.Pow(sp1.Y, 2));
                float abs = (float)(Math.Pow(sp1.X, 2) + Math.Pow(sp1.Y, 2));
                spArray[i] = Convert.ToDouble(abs);
            }
            */
        }

        #endregion

        public void CalculateDuration()
        {
            this.arrayMax = new List<double>();
            this.arrayDel = new List<double>();
            this.stackArrays = new List<List<double>>();
            this.stackDuration = new List<List<double>>();
            this.sort = new List<double>();
            this.sortD1 = new List<double>();
            this.sortD2 = new List<double>();
            this.lArrayDur = new List<double>();
            this.lArrayPeriod = new List<double>();
            this.arrayMinMaxElements = new List<int>();
            this.lArraysMiMax = new List<List<int>>();
            var count = 0;
            var startEl = 0;
            var endEl = 0;
            bool writeArray = false;

            try
            {
                this.max = this.amplitudeArray.Max();
                this.lowLevel = this.max * Threshold;
                this.firstLevel = this.max * 0.2;
                this.highLevel = this.max * 0.9;

                while (count < this.amplitudeArray.Count)
                {
                    if (this.amplitudeArray[count] > this.lowLevel)
                    {
                        startEl = count;
                        this.arrayMinMaxElements.Add(startEl);
                        count++;
                        while (this.amplitudeArray[count] > this.lowLevel)
                        {
                            endEl = count;
                            count++;
                        }

                        this.arrayMinMaxElements.Add(endEl);
                        this.lArraysMiMax.Add(this.arrayMinMaxElements);
                    }

                    arrayMinMaxElements = new List<int>();
                    count++;                    
                }

                foreach (var n in this.amplitudeArray)
                {
                    if (n > this.lowLevel)
                    {
                        this.arrayMax.Add(n);
                        writeArray = true;
                    }
                    else
                    {
                        this.arrayDel.Add(n);
                        writeArray = false;
                    }

                    if (writeArray)
                    {
                        this.arrayDel = new List<double>();
                        if (this.stackArrays.Count == 0)
                        {
                            this.stackArrays.Add(this.arrayMax);
                            this.stackDuration.Add(this.arrayMax);
                        }
                        else
                        {
                            var lastEl = this.stackArrays.Last();
                            if (lastEl != this.arrayMax)
                            {
                                this.stackArrays.Add(this.arrayMax);
                                this.stackDuration.Add(this.arrayMax);
                            }
                        }
                    }
                    else
                    {
                        this.arrayMax = new List<double>();
                        if (this.stackArrays.Count == 0)
                        {
                            this.stackArrays.Add(this.arrayDel);
                        }
                        else
                        {
                            var lastEl = this.stackArrays.Last();
                            if (lastEl != this.arrayDel)
                            {
                                this.stackArrays.Add(this.arrayDel);
                            }
                        }
                    }
                }

                for (var i = 0; i < this.stackDuration.Count; i++)
                {
                    this.sort = this.stackDuration[i];
                    var first = this.sort.First();
                    var last = this.sort.Last();

                    /* Удаление порезанных импульсов
                                        if (first < firstLevel && last < firstLevel) 
                                        {
                                            lArrayDur.Add(sort.Count * delay);
                                            
                                            sortD1 = stackArrays[i * 2];
                                            sortD2 = stackArrays[i * 2 + 1];
                                            lArrayPeriod.Add((sortD1.Count + sortD2.Count) * delay);
                                        }
                                        */
                    this.lArrayDur.Add(this.sort.Count * this.delay);

                    this.sortD1 = this.stackArrays[i * 2];
                    this.sortD2 = this.stackArrays[i * 2 + 1];
                    this.lArrayPeriod.Add((this.sortD1.Count + this.sortD2.Count) * this.delay);
                }

                this.OnSendDuration?.Invoke(this.lArrayDur.ToArray(), this.lArrayPeriod.ToArray());
            }
            catch { }
        }
    }
}
