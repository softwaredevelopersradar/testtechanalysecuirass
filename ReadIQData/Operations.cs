﻿namespace ReadIQData
{
    using System;
    using System.Collections.Generic;

    using MathNet.Numerics.IntegralTransforms;

    using NAudio.Dsp;

    using Complex = System.Numerics.Complex;

    class Operations
    {
        public static double[,] FormAnArray(string element, List<double> a) 
        {
            const int K = 0;
            double[,] form = new double[1, a.Count + 1];

            switch (element)
            {
                case "firstZero":
                    for (int i = 0; i < a.Count; i++)
                    {
                        form[K, i] = a[i];
                    }
                    break;
                case "lastZero":
                    for (int i = 1; i < a.Count + 1; i++)
                    {
                        form[K, i] = a[i - 1];
                    }
                    break;
            }

            return form;
        }

        public static double[] FormAGeterodyne(string element, int length) 
        {
            const int FazaRand = 90;
            const double Faza_0 = 0 * Math.PI / 180d;
            const double Faza_1 = 135 * Math.PI / 180d;
            const double delay = 0.0145416259765625;
            var timeArray = new double[length];
            var signal = new double[length];
            double[] FazaGet = new double[length];
            const double f0 = 58.768 * 1e6;
            var twoPiF0 = 2 * Math.PI * f0 + Faza_0;

            for(int i = 0; i < length; i++) 
            {
                timeArray[i] = delay * i;
                FazaGet[i] = Math.PI;
            }

            switch (element) 
            {
                case "signal_I":
                    for (int i = 0; i < length; i++)
                    {
                        signal[i] = Math.Cos(timeArray[i] * twoPiF0 + FazaGet[i]);
                    }
                    break;
                case "signal_Q":
                    for (int i = 0; i < length; i++)
                    {
                        signal[i] = Math.Sin(timeArray[i] * twoPiF0 + FazaGet[i]);
                    }
                    break;
                case "signal_2":
                    for (int i = 0; i < length; i++)
                    {
                        signal[i] = Math.Cos((2 * Math.PI * f0) * timeArray[i] + Faza_1);
                    }
                    break;
            }
            return signal;
        }

        public static Complex[] FormComplex(double[] dataI, double[] dataQ, int start, int end) 
        {
            Complex[] complex = new Complex[end - start + 1];
            int index = 0;
            for (int i = start; i <= end; i++) 
            {
                complex[index] = new Complex(dataI[i], dataQ[i]);
                index++;
            }
            
            return complex;
        }

        public static Complex[] ComplexMultiple(double[] dataPhase, Complex[] complex) 
        {
            Complex[] cmp = new Complex[complex.Length];
            for (int i = 0; i < complex.Length; i++)
            {
                cmp[i] = complex[i] * dataPhase[i];  //new Complex(complex[i].Real * dataPhase[i], complex[i].Imaginary * dataPhase[i])
            }
            return cmp;
        }

        private static NAudio.Dsp.Complex[] CalculateFFT(Complex[] complex) 
        {
            NAudio.Dsp.Complex[] complex1 = new NAudio.Dsp.Complex[complex.Length];
            for(int i = 0; i < complex.Length; i++) 
            {
                complex1[i].X = (float)complex[i].Real;
                complex1[i].Y = (float)complex[i].Imaginary;
            }
            FastFourierTransform.FFT(true, (int)Math.Log(complex.Length, 2.0), complex1);

            return complex1;
        }

        private static NAudio.Dsp.Complex[] CalculateInverseFFT(NAudio.Dsp.Complex[] complices) 
        {
            NAudio.Dsp.Complex[] complex1 = new NAudio.Dsp.Complex[complices.Length];

            //FastFourierTransform.FFT.
            return complices;
        }
 
        public static Complex[] CalculateFft(Complex[] complices) 
        {
            Fourier.Forward(complices, FourierOptions.Matlab);
            return complices;
        }

        public static Complex[] CalculateInverseFft(Complex[] complices) 
        {
            Fourier.Inverse(complices, FourierOptions.Matlab);
            return complices;
        }

        private static double[] FormADoubleSignal(double[] array) 
        {
            for(int i = 0; i < array.Length; i++) 
            {
                array[i] = Math.Pow(array[i], 2);
            }
            return array;
        }

        private static double[] FormASignal(double[] iq, double[] signal, double[] signalDouble) 
        {
            var array = new double[iq.Length];

            for (int i = 0; i < iq.Length; i++) 
            {
                array[i] = (iq[i] * signal[i]) - 1 * signalDouble[i];
            }
            return array;
        }

        private static double[] FormFTSignal(double[] signal_1, double[] signal_2) 
        {
            var arrayFT = new double[signal_1.Length];

            for(int i=0; i < signal_1.Length; i++) 
            {
                arrayFT[i] = (signal_1[i] + signal_2[i]) * (signal_1[i] - signal_2[i]);
            }
            return arrayFT;
        }

        private static double[,] Multiplication(double[,] a, double[,] b)
        {
            double[,] mult = new double[a.GetLength(0), b.GetLength(1)];

            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    mult[i, j] = 0;
                    for (int k = 0; k < a.GetLength(1); k++)
                    {
                        mult[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return mult;
        }
    }
}
