﻿using System;
using System.Windows;

namespace ReadIQFromFile
{
    public partial class MainWindow : Window
    {
        #region Top Panel
        /*
        private void TopPanelControl_OnCheckLocalProp(object sender, EventArgs e)
        {
            if (isCheckedLocal)
            {
                gridMain.ColumnDefinitions[0].Width = new GridLength(0);
                isCheckedLocal = false;
            }
            else
            {
                GridLength gridLength = (GridLength)myGridLengthConverter.ConvertFromString(width);
                GridLength gridLengthM = (GridLength)myGridLengthConverter.ConvertFromString(widthM);
                gridMain.ColumnDefinitions[0].Width = gridLength;
                gridMain.ColumnDefinitions[2].Width = gridLengthM;

                isCheckedLocal = true;
            }
        }

        private void TopPanelControl_OnStart(object sender, EventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                try
                {
                    Calculate.ReadDataRe(basicProperties.Local.Common.PathFileReal);
                    Calculate.ReadDataIm(basicProperties.Local.Common.PathFileImagine);

                    if (existsFileRe == true && existsFileIm == true)
                    {
                        Calculate.CalculateModulAmplitude();
                        Calculate.CalculatePhase();
                        Calculate.CalculateFreq();
                        Calculate.CalculateDuration();
                        //OpenControl.CalculateSP(); 
                    }
                }
                catch { }                 
            });
        }

        private void InitTopPanelControl()
        {
            if (isCheckedLocal)
            {
                GridLength gridLength = (GridLength)myGridLengthConverter.ConvertFromString(width);
                gridMain.ColumnDefinitions[0].Width = gridLength;
            }
            else { gridMain.ColumnDefinitions[0].Width = new GridLength(0); }

            TopPanelControl.OnCheckLocalProp += TopPanelControl_OnCheckLocalProp;
            TopPanelControl.OnStart += TopPanelControl_OnStart;
        }
        */
        #endregion

        private void StartCalculateFile() 
        {
            DispatchIfNecessary(() =>
            {
                try
                {
                    if (basicProperties.Local.Common.PathFileReal == basicProperties.Local.Common.PathFileImagine)
                    {
                        this.Calculate.ReadDataBoth(basicProperties.Local.Common.PathFileReal);
                    }
                    else
                    {
                        Calculate.ReadDataRe(basicProperties.Local.Common.PathFileReal);
                        Calculate.ReadDataIm(basicProperties.Local.Common.PathFileImagine);
                    }
                    if (existsFileRe && existsFileIm)
                    {
                        bAM.IsChecked = true;
                        Calculate.CalculateModulAmplitude((int)Modulation.AM);
                        Calculate.CalculatePhase((int)Modulation.AM);
                        Calculate.CalculateFreq((int)Modulation.AM);
                        Calculate.CalculateDuration();
                    }
                }
                catch { }
            });
        }
    }
}
