﻿using System.Windows;

namespace ReadIQFromFile
{
    public partial class MainWindow : Window
    {
        private double left = 0.0;
        private double Left 
        {
            get => left;
            set => left = value; 
        }

        private double width = 5.0;
        private double Width 
        {
            get => width;
            set => width = value;
        }

        private double right = 5.0;
        private double Right 
        {
            get => right;
            set => right = value;
        }

        private void InitPanoramControl()
        {
            Panorams.OnBandData += Panorams_OnBandData;
            Panorams.OnBandPhase += Panorams_OnBandPhase;
            Panorams.OnBandFreq += Panorams_OnBandFreq;

            Panorams.OnSendCoord += Panorams_OnSendCoord;
            Panorams.OnSendCoordPhase += Panorams_OnSendCoordPhase;
            Panorams.OnSendCoordFreq += Panorams_OnSendCoordFreq;
        }
        #region BandEvents
        private void Panorams_OnBandData(double bLeft, double bWidth, double bRight)
        {
            string textW = string.Format("{0}", bWidth.ToString());
            string textL = string.Format("{0}", bLeft.ToString());
            string textR = string.Format("{0}", bRight.ToString());

            Left = bLeft;
            Width = bWidth;
            Right = bRight;

            DispatchIfNecessary(() => 
            {
                if (existsFileRe == true && existsFileIm == true)
                {
                    DrawPartOfData(Left, Width, Right);
                }
            });
        }

        private void DrawPartOfData(double bLeft, double bWidth, double bRight) 
        {
            PanoramsPart.DrawAmp(Calculate.amplitudeArray.ToArray(), bLeft, bWidth, bRight);
            PanoramsPart.DrawPhase(Calculate.phaseArray.ToArray(), bLeft, bWidth, bRight);
            PanoramsPart.DrawFreq(Calculate.freqArray.ToArray(), bLeft, bWidth, bRight);
        }

        private void Panorams_OnBandFreq(double bLeft, double bWidth, double bRight)
        {
            string textW = string.Format("{0}", bWidth.ToString());
            string textL = string.Format("{0}", bLeft.ToString());
            string textR = string.Format("{0}", bRight.ToString());
        }

        private void Panorams_OnBandPhase(double bLeft, double bWidth, double bRight)
        {
            string textW = string.Format("{0}", bWidth.ToString());
            string textL = string.Format("{0}", bLeft.ToString());
            string textR = string.Format("{0}", bRight.ToString());
        }
        #endregion

        #region CoordEvents
        private void Panorams_OnSendCoordFreq(double xCursor, double yCursor)
        {
            string textX = string.Format("{0}", xCursor.ToString());
            string textY = string.Format("{0}", yCursor.ToString());
        }

        private void Panorams_OnSendCoordPhase(double xCursor, double yCursor)
        {
            string textX = string.Format("{0}", xCursor.ToString());
            string textY = string.Format("{0}", yCursor.ToString());
        }

        private void Panorams_OnSendCoord(double xCursor, double yCursor)
        {
            string textX = string.Format("{0}", xCursor.ToString());
            string textY = string.Format("{0}", yCursor.ToString());
        }
        #endregion

        private void FindMax(double[] array)
        {
            max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (max < array[i])
                {
                    max = array[i];
                }
            }
        }

        private void FindMin(double[] array)
        {
            min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (min > array[i])
                {
                    min = array[i];
                }
            }
        }
        
        private void FindAverage(double[] array) 
        {
            average = 0;
            double sum = 0;
            for(int i = 0; i < array.Length; i++) 
            {
                sum = sum + array[i];
            }
            average = sum / array.Length;
        }
    }
}
