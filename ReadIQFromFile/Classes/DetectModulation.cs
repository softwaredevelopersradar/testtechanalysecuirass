﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ReadIQFromFile
{
    public partial class MainWindow
    {
        enum Modulation 
        {
            AM = 0,
            FSK = 1,
            PSK = 2
        }
        private void BAM_Click(object sender, RoutedEventArgs e)
        {
            if (bAM.IsChecked.Value)
            {
                bPSK.IsChecked = false;
                bFSK.IsChecked = false;
                Calculate.CalculateFreq((ReadIQData.Calculate.Modulation)Modulation.AM);
                Calculate.CalculatePhase((ReadIQData.Calculate.Modulation)Modulation.AM);
                DrawPartOfData(Left, Width, Right);
                //Calculate.CalculateModulAmplitude((ReadIQData.Calculate.Modulation)Modulation.AM);
                //Panorams.DrawAmp(Calculate.amplitudeArray);
            }
        }

        private void BPSK_Click(object sender, RoutedEventArgs e)
        {
            if (bPSK.IsChecked.Value)
            {
                bAM.IsChecked = false;
                bFSK.IsChecked = false;
                Calculate.CalculateFreq((ReadIQData.Calculate.Modulation)Modulation.AM);
                Calculate.CalculatePhase((ReadIQData.Calculate.Modulation)Modulation.PSK);
                DrawPartOfData(Left, Width, Right);
                //Calculate.CalculateModulAmplitude((ReadIQData.Calculate.Modulation)Modulation.PSK);
                //Panorams.DrawAmp(Calculate.amplitudeArray);
            }
        }

        private void BFSK_Click(object sender, RoutedEventArgs e)
        {
            if (bFSK.IsChecked.Value)
            {
                bAM.IsChecked = false;
                bPSK.IsChecked = false;
                Calculate.CalculateFreq((ReadIQData.Calculate.Modulation)Modulation.FSK);
                Calculate.CalculatePhase((ReadIQData.Calculate.Modulation)Modulation.AM);
                DrawPartOfData(Left, Width, Right);
                //Calculate.CalculateModulAmplitude((ReadIQData.Calculate.Modulation)Modulation.AM);
                //Panorams.DrawAmp(Calculate.amplitudeArray);
            }
        }
    }
}
