﻿using DllControlProperties.Models;
using System;
using System.IO;
using YamlDotNet.Serialization;

namespace ReadIQFromFile
{
    public partial class MainWindow
    {
        public LocalProperties YamlLoad()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader("LocalProperties.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var localproperties = new LocalProperties();
            try
            {
                localproperties = deserializer.Deserialize<LocalProperties>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (localproperties == null)
            {
                localproperties = GenerateDefaultLocalProperties();
                YamlSave(localproperties);
            }
            return localproperties;

        }

        private LocalProperties GenerateDefaultLocalProperties()
        {
            var localProperties = new LocalProperties();

            return localProperties;
        }

        public void YamlSave(LocalProperties localProperties)
        {
            try
            {
                var serializer = new SerializerBuilder().Build();
                var yaml = serializer.Serialize(localProperties);

                using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "LocalProperties.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
