﻿using DllControlProperties.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadIQFromFile
{
    public partial class MainWindow
    {
        LocalProperties LocalProperties;

        public void InitLocalProperties()
        {
            LocalProperties = YamlLoad();
            SetLocalProperties();
        }

        private void SetLocalProperties()
        {
            try
            {
                basicProperties.Local = LocalProperties;
            }
            catch { }
        }

        private void basicProperties_OnLocalPropertiesChanged(object sender, LocalProperties arg)
        {
            StartCalculateFile();
            YamlSave(arg);            
        }

    }
}
