﻿using DllControlProperties.Models;

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace ReadIQFromFile
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ReadIQData.Calculate Calculate;

        bool existsFileRe = false;
        bool existsFileIm = false;
        int countImpulse;
        double allDuration;
        double delay = 0.0145416259765625;
        double max;
        double min;
        double average;

        public MainWindow()
        {
            InitializeComponent();
            
            Calculate = new ReadIQData.Calculate();

            
            InitLocalProperties();
            SetLanguage(basicProperties.Local.Common.Language);
            PanoramsPart.NameAxP = "\u03C6," + "\u00B0";
            Panorams.NameAxP = "\u03C6," + "\u00B0";
            HistOne.NameAx = "%";
            HistPeriod.NameAx = "%";
            //tabDuration.Header = "Duration" + "\u03C4";

            Panorams.OnSendThreshold += Panorams_OnSendThreshold;
            Panorams.OnSubmittedFimpValue += Panorams_OnSubmittedFimpValue;

            Calculate.OnCalculate += OpenControl_OnCalculate;
            Calculate.OnCalculateFreq += Calculate_OnCalculateFreq;
            Calculate.OnCalculatePhase += Calculate_OnCalculatePhase;
            Calculate.OnCalculatedPSK += Calculate_OnCalculatedPSK;

            Calculate.OnSpectrogramSize += OpenControl_OnSpectrogramSize;
            Calculate.OnSendDuration += OpenControl_OnSendDuration;
            Calculate.OnSendExistsFilePathRe += Calculate_OnSendExistsFilePathRe;
            Calculate.OnSendExistsFilePathIm += Calculate_OnSendExistsFilePathIm;
            Calculate.OnSendFreqStat += Calculate_OnSendFreqStat;
                        
            InitPanoramControl();                      
            InitMarkup();
            StartCalculateFile();

            
        }

        private void Panorams_OnSubmittedFimpValue(string text)
        {
            Calculate.F_IMP = Convert.ToDouble(text); // * 1e6
        }

        private void Panorams_OnSendThreshold(double threshold)
        {
            Calculate.Threshold = threshold;
            //StartCalculateFile();
        }

        private void Calculate_OnCalculatedPSK(double[] array)
        {
            Panorams.DrawAmp(array);
        }

        private void Calculate_OnSendFreqStat(double min, double max, double average)
        {
            basicProperties.Local.Statistic.MinPulseFrequency = (float)min;
            basicProperties.Local.Statistic.MaxPulseFrequency = (float)max;
            basicProperties.Local.Statistic.AverageFrequency = (float)average;
        }

        private void Calculate_OnSendExistsFilePathIm(bool Exists)
        {
            existsFileIm = Exists;
        }

        private void Calculate_OnSendExistsFilePathRe(bool Exists)
        {
            existsFileRe = Exists;
        }

        private void OpenControl_OnSendDuration(double[] duration, double[] period)
        {
            HistOne.DrawGraph(duration);
            HistPeriod.DrawGraph(period);

            countImpulse = duration.Length;
            basicProperties.Local.Statistic.Pulses = countImpulse;  //Count Impulses

            FindMin(duration);
            FindMax(duration);
            FindAverage(duration);
            basicProperties.Local.Statistic.MinPulseDuration = (float)min;
            basicProperties.Local.Statistic.MaxPulseDuration = (float)max;
            basicProperties.Local.Statistic.AveragePulseDuration = (float)average;

            FindMin(period);
            FindMax(period);
            FindAverage(period);
            basicProperties.Local.Statistic.MinPulsePeriod = (float)min;
            basicProperties.Local.Statistic.MaxPulsePeriod = (float)max;
            basicProperties.Local.Statistic.AveragePulsePeriod = (float)average;
        }

        private void OpenControl_OnSpectrogramSize(int length)
        {
            Panorams.LengthSG = length;
        }

        private void OpenControl_OnCalculate(object sender, EventArgs e)
        {
            Panorams.DrawAmp(Calculate.amplitudeArray.ToArray()); 

            allDuration = (Calculate.amplitudeArray.ToArray().Length * delay) / 1000000.0;
            basicProperties.Local.Statistic.TimeRecord = (float)allDuration;  //Time Record
        }

        private void Calculate_OnCalculatePhase(object sender, EventArgs e)
        {
            Panorams.DrawGraphPhase(Calculate.phaseArray.ToArray());
        }

        private void Calculate_OnCalculateFreq(object sender, EventArgs e)
        {
            Panorams.DrawGraphFreq(Calculate.freqArray.ToArray());
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        private void basicProperties_OnLanguageChanged(object sender, Languages e)
        {
            SetLanguage(e);
        }

        private void Panorams_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void PanoramsPart_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
