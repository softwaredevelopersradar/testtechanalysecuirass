﻿using DllControlProperties.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ReadIQFromFile
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private void SetLanguage(Languages languages) 
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (languages)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/ReadIQFromFile;component/Translate/Lang.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/ReadIQFromFile;component/Translate/Lang.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/ReadIQFromFile;component/Translate/Lang.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                              where d.Source != null && d.Source.OriginalString.StartsWith("/ReadIQFromFile;component/Translate/Lang.")
                                              select d).First();

                if (oldDict != null)
                {
                    int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                    Resources.MergedDictionaries.Remove(oldDict);
                    Resources.MergedDictionaries.Insert(ind, dict);

                }
                else
                {
                    Resources.MergedDictionaries.Add(dict);
                }
            }
            catch (Exception ex)
            { }
        }
    }
}
