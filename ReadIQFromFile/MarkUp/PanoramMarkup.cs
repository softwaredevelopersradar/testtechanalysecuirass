﻿using System.ComponentModel;

namespace ReadIQFromFile
{
    public class WindowMarkup : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public WindowMarkup() 
        {
            HistogramMarkupSettings = new HistogramMarkupSettings();
            PropertyMarkupSettings = new PropertyMarkupSettings();
            PanoramasMarkupSettings = new PanoramasMarkupSettings();
        }
        public HistogramMarkupSettings HistogramMarkupSettings { get; set; }
        public PropertyMarkupSettings PropertyMarkupSettings { get; set; }
        public PanoramasMarkupSettings PanoramasMarkupSettings { get; set; }
    }

    public class HistogramMarkupSettings : IMurkupSettings
    {
        public HistogramMarkupSettings() 
        {
            PrevSize = 200;
            CurrentSize = 200;
            IsVisible = false;
        }
        public double PrevSize { get; set; }
        public double CurrentSize { get; set; }
        public bool IsVisible { get; set; }
    }

    public class PropertyMarkupSettings : IMurkupSettings
    {
        public PropertyMarkupSettings() 
        {
            PrevSize = 400;
            CurrentSize = 400;
            IsVisible = false;
        }
        public double PrevSize { get; set; }
        public double CurrentSize { get; set; }
        public bool IsVisible { get; set; }
    }

    public class PanoramasMarkupSettings: IMurkupSettings 
    {
        public PanoramasMarkupSettings() 
        {
            PrevSize = 400;
            CurrentSize = 400;
            IsVisible = false;
        }
        public double PrevSize { get; set; }
        public double CurrentSize { get; set; }
        public bool IsVisible { get; set; }
    }

    class DefaultMarkup 
    {
        public const double HeightPanelHistogram = 200;
        public const double WidthPanelProperty = 400;
        public const double WidthPanelPanoramas = 300;
    }
}
