﻿using System;
using System.IO;
using System.Text;
using YamlDotNet.Serialization;

namespace ReadIQFromFile
{
    public partial class MainWindow
    {
        public WindowMarkup YamlLoadMarkup() 
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "MarkupWnd.yaml", Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();
            try
            {
                windowMarkup = deserializer.Deserialize<WindowMarkup>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (windowMarkup == null)
            {
                windowMarkup = SetDefaultMarkup();
                YamlSaveMarkup(windowMarkup);
            }
            return windowMarkup;
        }

        private WindowMarkup SetDefaultMarkup()
        {
            windowMarkup = new WindowMarkup();

            windowMarkup.HistogramMarkupSettings.IsVisible = true;
            windowMarkup.HistogramMarkupSettings.PrevSize = DefaultMarkup.HeightPanelHistogram;
            windowMarkup.HistogramMarkupSettings.CurrentSize = DefaultMarkup.HeightPanelHistogram;

            windowMarkup.PropertyMarkupSettings.IsVisible = true;
            windowMarkup.PropertyMarkupSettings.PrevSize = DefaultMarkup.WidthPanelProperty;
            windowMarkup.PropertyMarkupSettings.CurrentSize = DefaultMarkup.WidthPanelProperty;

            windowMarkup.PanoramasMarkupSettings.IsVisible = true;
            windowMarkup.PanoramasMarkupSettings.PrevSize = DefaultMarkup.WidthPanelProperty;
            windowMarkup.PanoramasMarkupSettings.CurrentSize = DefaultMarkup.WidthPanelProperty;
            return windowMarkup;
        }

        public void YamlSaveMarkup(WindowMarkup windowMarkup)
        {
            try
            {
                var serializer = new SerializerBuilder().Build();
                var yaml = serializer.Serialize(windowMarkup);

                using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "MarkupWnd.yaml", false, Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
