﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadIQFromFile
{
    public interface IMurkupSettings
    {
        double PrevSize { get; set; }
        double CurrentSize { get; set; }
        bool IsVisible { get; set; }
    }
}
