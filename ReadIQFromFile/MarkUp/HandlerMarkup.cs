﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ReadIQFromFile
{
    public partial class MainWindow
    {
        WindowMarkup windowMarkup;
        
        private void InitMarkup() 
        {
            windowMarkup = YamlLoadMarkup();
            ShowPanelHistogram();
            ShowPanelProperty();
            ShowPanelPanoramas();
        }
        private void GridSplitterHistogram_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            windowMarkup.HistogramMarkupSettings.PrevSize = windowMarkup.HistogramMarkupSettings.CurrentSize;
            windowMarkup.HistogramMarkupSettings.CurrentSize = gridRtf.RowDefinitions[2].Height.Value;
            windowMarkup.HistogramMarkupSettings.IsVisible = true;
            YamlSaveMarkup(windowMarkup);
        }

        private void GridSplitterProperty_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e) 
        {
            windowMarkup.PropertyMarkupSettings.PrevSize = windowMarkup.PropertyMarkupSettings.CurrentSize;
            windowMarkup.PropertyMarkupSettings.CurrentSize = gridMain.ColumnDefinitions[0].Width.Value;
            windowMarkup.PropertyMarkupSettings.IsVisible = true;
            YamlSaveMarkup(windowMarkup);
        }

        private void GridSplitterPanoramas_IsMouseCapturedChanged(object sender, DependencyPropertyChangedEventArgs e) 
        {
            windowMarkup.PanoramasMarkupSettings.PrevSize = windowMarkup.PanoramasMarkupSettings.CurrentSize;
            windowMarkup.PanoramasMarkupSettings.CurrentSize = gridPanoramas.ColumnDefinitions[2].Width.Value;
            windowMarkup.PanoramasMarkupSettings.IsVisible = true;
            YamlSaveMarkup(windowMarkup);
        }

        private void BOpenRTF_Click(object sender, RoutedEventArgs e)
        {
            if (bOpenRTF.IsChecked.Value)
            {
                windowMarkup.HistogramMarkupSettings.IsVisible = true;
                windowMarkup.HistogramMarkupSettings.CurrentSize = windowMarkup.HistogramMarkupSettings.PrevSize;
            }
            else
            {
                windowMarkup.HistogramMarkupSettings.IsVisible = false;
                windowMarkup.HistogramMarkupSettings.PrevSize = windowMarkup.HistogramMarkupSettings.CurrentSize;
                windowMarkup.HistogramMarkupSettings.CurrentSize = 0;
            }
            YamlSaveMarkup(windowMarkup);
            ShowPanelHistogram();
        }

        private void BProperty_Click(object sender, RoutedEventArgs e)
        {
            if (bProperty.IsChecked.Value)
            {
                windowMarkup.PropertyMarkupSettings.IsVisible = true;
                windowMarkup.PropertyMarkupSettings.CurrentSize = windowMarkup.PropertyMarkupSettings.PrevSize;
            }
            else 
            {
                windowMarkup.PropertyMarkupSettings.IsVisible = false;
                windowMarkup.PropertyMarkupSettings.PrevSize = windowMarkup.PropertyMarkupSettings.CurrentSize;
                windowMarkup.PropertyMarkupSettings.CurrentSize = 0;
            }
            YamlSaveMarkup(windowMarkup);
            ShowPanelProperty();
        }

        private void BDefault_Click(object sender, RoutedEventArgs e)
        {   
            if (windowMarkup.HistogramMarkupSettings.IsVisible || windowMarkup.PropertyMarkupSettings.IsVisible || windowMarkup.PanoramasMarkupSettings.IsVisible)
            {
                SetDefaultMurkup();
            }
        }

        private void ShowPanelHistogram() 
        {
            try 
            {
                if (!windowMarkup.HistogramMarkupSettings.IsVisible)
                {
                    gridRtf.RowDefinitions[2].Height = new GridLength(0);
                    bOpenRTF.IsChecked = false;
                }
                else 
                {                    
                    gridRtf.RowDefinitions[2].Height = new GridLength(windowMarkup.HistogramMarkupSettings.CurrentSize);
                    bOpenRTF.IsChecked = true;
                }
            }
            catch { }
        }

        private void ShowPanelProperty() 
        {
            try
            {
                if (!windowMarkup.PropertyMarkupSettings.IsVisible)
                {
                    gridMain.ColumnDefinitions[0].Width = new GridLength(0);
                    bProperty.IsChecked = false;
                }
                else 
                {
                    gridMain.ColumnDefinitions[0].Width = new GridLength(windowMarkup.PropertyMarkupSettings.CurrentSize);
                    bProperty.IsChecked = true;
                }
            }
            catch { }
        }

        private void ShowPanelPanoramas() 
        {
            try
            {
                if (!windowMarkup.PanoramasMarkupSettings.IsVisible) 
                {
                    gridPanoramas.ColumnDefinitions[2].Width = new GridLength(0);
                }
                else 
                {
                    gridPanoramas.ColumnDefinitions[2].Width = new GridLength(windowMarkup.PanoramasMarkupSettings.CurrentSize);
                }
            }
            catch { }
        }

        public void SetDefaultMurkup()
        {
            windowMarkup.HistogramMarkupSettings.IsVisible = true;
            windowMarkup.HistogramMarkupSettings.PrevSize = DefaultMarkup.HeightPanelHistogram;
            windowMarkup.HistogramMarkupSettings.CurrentSize = DefaultMarkup.HeightPanelHistogram;

            windowMarkup.PropertyMarkupSettings.IsVisible = true;
            windowMarkup.PropertyMarkupSettings.PrevSize = DefaultMarkup.WidthPanelProperty;
            windowMarkup.PropertyMarkupSettings.CurrentSize = DefaultMarkup.WidthPanelProperty;

            windowMarkup.PanoramasMarkupSettings.IsVisible = true;
            windowMarkup.PanoramasMarkupSettings.PrevSize = DefaultMarkup.WidthPanelPanoramas;
            windowMarkup.PanoramasMarkupSettings.CurrentSize = DefaultMarkup.WidthPanelPanoramas;

            YamlSaveMarkup(windowMarkup);
            ShowPanelHistogram();
            ShowPanelProperty();
            ShowPanelPanoramas();
        }
    }
}
